describe('Vilniustransport log in tests', () => {

    beforeEach(() => {
        const emailField = $('.input-group [name="username"]');

        if (emailField.isDisplayed() === false) {
            browser.maximizeWindow();
            browser.url('https://beta.vilniustransport.lt/');

            browser.pause(3000); //waiting until animation ends
            const cookieButton = $('[id="ccc-close"]');
            cookieButton.waitForClickable();
            cookieButton.click();

            const testiNarsymaButton = $('[class="modal-content"] .btn-primary');
            testiNarsymaButton.waitForClickable();
            testiNarsymaButton.click();

            const eSavitarnaButton = $('#navbarTop ul li:nth-child(4) a');
            eSavitarnaButton.waitForDisplayed();
            eSavitarnaButton.click();
        }
    });

    afterEach(() => {
        const vilniecioKortelesButton = $('[class="btn btn-secondary w-icon-left active"]');

        if (vilniecioKortelesButton.isDisplayed()) {
            const userMenu = $('[id="dropdownMenuButton"]');
            userMenu.click();
            const logoutButton = $('[aria-labelledby="dropdownMenuButton"] a:nth-child(4)');
            logoutButton.waitForDisplayed();
            logoutButton.click();
        }
    });

    it('should login with valid data', () => {

        const emailField = $('.input-group [name="username"]');
        emailField.waitForDisplayed();
        emailField.setValue('benjaminCubensis@gmail.com');

        const passwordFiled = $('.input-group [name="password"]');
        passwordFiled.waitForDisplayed();
        passwordFiled.setValue('AmanitaMuscaria92');

        const prisijungtiButton = $('[name="login_submit"]');
        prisijungtiButton.waitForDisplayed();
        prisijungtiButton.click();

        const vilniecioKorteleMenu = $('[class="btn btn-secondary w-icon-left active"]');
        expect(vilniecioKorteleMenu.getText()).toEqual('Vilniečio kortelės');
    });

    it('should not login with invalid email and valid password', () => {

        const emailField = $('.input-group [name="username"]');
        emailField.waitForDisplayed();
        emailField.setValue('benjaminCubensis@gmail');

        const passwordFiled = $('.input-group [name="password"]');
        passwordFiled.waitForDisplayed();
        passwordFiled.setValue('AmanitaMuscaria92');

        const prisijungtiButton = $('[name="login_submit"]');
        prisijungtiButton.waitForDisplayed();
        prisijungtiButton.click();

        const errorMessage = $('[class="alert alert-danger"]');
        errorMessage.waitForDisplayed();
        expect(errorMessage.getText()).toEqual('Vartotojas nėra užregistruotas jūsų įvestu el. pašto adresu');
    });

    it('should not login with valid email and invalid password', () => {

        const emailField = $('.input-group [name="username"]');
        emailField.waitForDisplayed();
        emailField.setValue('benjaminCubensis@gmail.com');

        const passwordFiled = $('.input-group [name="password"]');
        passwordFiled.waitForDisplayed();
        passwordFiled.setValue('password');

        const prisijungtiButton = $('[name="login_submit"]');
        prisijungtiButton.waitForDisplayed();
        prisijungtiButton.click();

        const errorMessage = $('[class="alert alert-danger"]');
        errorMessage.waitForDisplayed();
        expect(errorMessage.getText()).toEqual('Įvestas neteisingas slaptažodis.');
    });
});